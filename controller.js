// link api
const Base_URL = "https://637af59f10a6f23f7f9bf2cc.mockapi.io/";

// function get data
function getData() {
    axios({
        url: `${Base_URL}Products`,
        method: "GET",
    })
        .then(function (res) {
            console.log("res:", res.data);
            renderProducts(res.data);
        })
        .catch(function (err) {
            console.log("err: ", err);
        });

}


// hiện sản phẩm trên màn hình, và giỏ hàng

let allBillbefore = [];
let renderProducts = (data) => {
    contentHtml1 = ``;
    contentHtml2 = ``;
    for (let i = 0; i < data.length; i++) {

        contentHtml1 += `
        <div id="Content_${data[i].id}" class="card_items col-3 text-center p-3">
            <div class="card_img" >
                <img src=${data[i].img}>
            </div>
            
            <div class="card_title mb-1">
                <h3 >Loại: <span id="TypeProduct${data[i].id}">${data[i].type}</span></h3>
                <h3>Tên: ${data[i].name}</h3>
                <h3>Giá: ${data[i].price} $</h3>
                <p>Màn hình: ${data[i].screen}</p>
                <p>Camera sau: ${data[i].backCamera}</p>
                <p>Camera trước: ${data[i].frontCamera}</p>
                <p>Miêu tả: ${data[i].desc}</p>

                <button id="clickbuy${data[i].id}" onclick="buyProduct(${data[i].id})" type="button" class="btn btn-primary btn-sm my-1"><i class="fa fa-shopping-cart"></i></button>

                <div id="buysProduct(${data[i].id})" class="justify-content-between align-items-center" style="font-size: 20px; display: none;">

                    <button onclick="Giam(${data[i].id})" type="button" class="btn btn-primary btn-sm"><i class="fa fa-chevron-left"></i></button>

                    <span id="numberbuy${data[i].id}" class="buysProduct">0</span>

                    <button onclick="Tang(${data[i].id})" type="button" class="btn btn-primary btn-sm"><i class="fa fa-chevron-right"></i></button>

                </div>

            </div>

            <button onclick="Xoa(${data[i].id})" id="Xoa${data[i].id}" type="button" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></button>
        </div>
        `
        contentHtml2 += `
        <div id="modal_${data[i].id}" class="modal_item justify-content-between align-items-center">
            <div class="modal_img">
                <img src=${data[i].img}>
            </div>

            <strong class="name">${data[i].name}</strong>

            <span class="qty-change">

                <div class="d-flex justify-content-around align-items-center">

                    <button onclick="Giam(${data[i].id})" type="button" class="btn btn-primary btn-sm"><i class="fa fa-chevron-left"></i></button>

                    <span id="Numbermodal_${data[i].id}" class="mx-3">0</span>

                    <button onclick="Tang(${data[i].id})" type="button" class="btn btn-primary btn-sm"><i class="fa fa-chevron-right"></i></button>

                </div>
            </span>

            <p id="pricemodal_${data[i].id}" class="price">$ ${data[i].price}</p>

            <button onclick="Xoa(${data[i].id})" class="btn btn-primary btn-sm"><i
                class="fas fa-trash"></i>
            </button>
        </div>
        `


    }

    document.getElementById("showProducts").innerHTML = contentHtml1;
    document.getElementById("buyProduct_modal").innerHTML = contentHtml2;

    let sumnumberCart = 0;
    for (let i = 0; i < data.length; i++) {
        renderLocalstorage(i);
        sumnumberCart += Billrender[i].number;
    };
    if (sumnumberCart > 0) {
        document.getElementById("numberCart").innerHTML = sumnumberCart;
        document.getElementById("numberCart").className = "mx-5";
    }
}


// function render số lượng sản phẩm cạnh giỏ hàng
let rendernumberCart = () => {
    let arryBuysProduct = document.querySelectorAll(".buysProduct");
    let SumbuysProduct = 0;
    for (i = 0; i < arryBuysProduct.length; i++) {
        SumbuysProduct = SumbuysProduct + Number(arryBuysProduct[i].innerHTML);
    }
    document.getElementById('numberCart').innerHTML = SumbuysProduct;
    document.getElementById('numberCart').className = "mx-5";
    if (SumbuysProduct == 0) {
        document.getElementById('numberCart').className = "mx-5 d-none";
    }

}

// hiện tăng giảm số lượng mua
let buyProduct = (a) => {
    document.getElementById(`buysProduct(${a})`).style.display = "flex";
    document.getElementById(`Xoa${a}`).style.display = "inline";
    document.getElementById(`modal_${a}`).style.display = "flex";
    Tang(a);

}



// function tăng, giảm, xoá số lượng đã cho vào giỏ hàng ở card sản phẩm
let Giam = (a) => {
    let Numberbuy = Number(document.getElementById(`numberbuy${a}`).innerHTML);


    if (Numberbuy > 0) {
        Numberbuy = Numberbuy - 1;
        if (Numberbuy == 0) {
            document.getElementById(`buysProduct(${a})`).style.display = "none";
            document.getElementById(`Xoa${a}`).style.display = "";
            document.getElementById(`modal_${a}`).style.display = "none";
        }
        document.getElementById(`numberbuy${a}`).innerHTML = Numberbuy;
        document.getElementById(`Numbermodal_${a}`).innerHTML = Numberbuy;
        removeBill(a, Numberbuy)
    }


    rendernumberCart();

}

let Tang = (a) => {
    let Numberbuy = Number(document.getElementById(`numberbuy${a}`).innerHTML);
    Numberbuy = Numberbuy + 1;

    document.getElementById(`numberbuy${a}`).innerHTML = Numberbuy;
    document.getElementById(`Numbermodal_${a}`).innerHTML = Numberbuy;

    removeBill(a, Numberbuy);
    rendernumberCart();

}

let Xoa = (a) => {


    document.getElementById(`buysProduct(${a})`).style.display = "none";
    document.getElementById(`modal_${a}`).style.display = "none";
    document.getElementById(`Xoa${a}`).style.display = "";


    document.getElementById(`numberbuy${a}`).innerHTML = 0;
    document.getElementById(`Numbermodal_${a}`).innerHTML = 0;

    removeBill(a, 0);

    rendernumberCart();

}

// function filter, phải đổi id trong object bắt đầu từ số không
let filter = () => {
    let Valuefilter = document.getElementById("Valuefilter").value;
    let TypeProduct = (document.querySelectorAll(".card_title"));

    for (let i = 0; i < TypeProduct.length; i++) {
        document.getElementById(`Content_${i}`).style.display = "";
        if (Valuefilter == 2) {
            if (document.getElementById(`TypeProduct${i}`).innerHTML == "Samsung") {
                document.getElementById(`Content_${i}`).style.display = "none";
            }
        }
        if (Valuefilter == 3) {
            if (document.getElementById(`TypeProduct${i}`).innerHTML == "Iphone") {
                document.getElementById(`Content_${i}`).style.display = "none";
            }
        }
    }
}



// lưu dữ liệu vào localStorage
const ALLBILL = "ALLBILL";
function saveLocalstorage(a) {
    let jsonBill = JSON.stringify(a);
    localStorage.setItem(ALLBILL, jsonBill);
}



// class billProduct ghi sản phẩm và số lượng sản phẩm đã lên bill
function billProduct(id, number) {
    this.id = id;
    this.number = number;
}



// remove số lượng các bill khi thay đổi
// tạo tổng hợp các bill
let removeBill = (id, number) => {
    allBillbefore[id].number = number;
    console.log("allbillbefore2: ",allBillbefore)
    saveLocalstorage(allBillbefore);
    console.log("removebill: ", JSON.parse(localStorage.getItem(ALLBILL)));
}



// render localstorage 
let Billrender = [];
let renderLocalstorage = (a) => {
    Billrender = JSON.parse(localStorage.getItem(ALLBILL));
    if (Billrender[a].number > 0) {
        document.getElementById(`numberbuy${a}`).innerHTML = Billrender[a].number;
        document.getElementById(`buysProduct(${a})`).style.display = "flex";
        document.getElementById(`Xoa${a}`).style.display = "inline";
        document.getElementById(`modal_${a}`).style.display = "flex";
        document.getElementById(`Numbermodal_${a}`).innerHTML = Billrender[a].number;
    }
}
