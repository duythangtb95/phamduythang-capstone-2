// link api
const Base_URL = "https://637af59f10a6f23f7f9bf2cc.mockapi.io/";




let Products = [];
// function get data
const getData = () => {

    axios({
        url: `${Base_URL}Products`,
        method: "GET",
    })
        .then((res) => {
            Products = res.data;
            renderProducts(Products);
            console.log("product1: ", Products);
        })
        .catch(function (err) {
            console.log("err: ", err);
        });
}


// function render 
const renderProducts = (data) => {
    contentHtml1 = ``;

    for (let i = 0; i < data.length; i++) {
        contentHtml1 += `
        <div id="Content_${data[i].id}" class="card_items col-3 text-center p-3">
            <div class="card_img" >
                <img src=${data[i].img}>
            </div>
            
            <div class="card_title mb-1">
                <h3 >Loại: <span id="TypeProduct${data[i].id}">${data[i].type}</span></h3>
                <h3>Tên: ${data[i].name}</h3>
                <h3>Giá: ${data[i].price} $</h3>
                <p>Màn hình: ${data[i].screen}</p>
                <p>Camera sau: ${data[i].backCamera}</p>
                <p>Camera trước: ${data[i].frontCamera}</p>
                <p>Miêu tả: ${data[i].desc}</p>

                <button onclick="getinforupdateProduct(${i})" type="button" class="btn btn-primary btn-sm my-1" data-toggle="modal" data-target="#exampleModal">Cập nhật</button>

                <button onclick="Delete(${data[i].id})" type="button" class="btn btn-primary btn-sm">Xoá</button>
            </div>
        </div>
        `
    }

    document.getElementById("showProducts").innerHTML = contentHtml1;
};
getData();


// function filter 
let filter = () => {
    let Valuefilter = document.getElementById("Valuefilter").value;
    let TypeProduct = (document.querySelectorAll(".card_title"));



    for (let i = 0; i < TypeProduct.length; i++) {
        document.getElementById(`Content_${i}`).style.display = "";
        if (Valuefilter == 2) {
            if (document.getElementById(`TypeProduct${i}`).innerHTML == "Samsung") {
                document.getElementById(`Content_${i}`).style.display = "none";
            }
        }
        if (Valuefilter == 3) {
            if (document.getElementById(`TypeProduct${i}`).innerHTML == "Iphone") {
                document.getElementById(`Content_${i}`).style.display = "none";
            }
        }
    }
}


// lấy giá trị show lên form điền thông tin

let getinforupdateProduct = (i) => {
    console.log(Products);
    document.getElementById("getId").innerHTML = Products[i].id;
    document.getElementById("getName").value = Products[i].name;
    document.getElementById("getType").value = Products[i].type;
    document.getElementById("getPrice").value = Products[i].price;
    document.getElementById("getImg").value = Products[i].img;
    document.getElementById("getScreen").value = Products[i].screen;
    document.getElementById("getfrontCam").value = Products[i].frontCamera;
    document.getElementById("getbackCam").value = Products[i].backCamera;
    document.getElementById("getDesc").value = Products[i].desc;
    document.getElementById("buttonUpdate").innerHTML = "Cập nhật";
}

// let updateinforProduct = (first) => { second }
// function thêm addtoProducts()
let addtoProducts = () => {
    document.getElementById("getId").innerHTML = "";
    document.getElementById("resetForm").click();
    document.getElementById("buttonUpdate").innerHTML = "Thêm";
}

// object product
class newProducts {
    constructor(id, name, price, screen, backCamera, frontCamera, img, desc, type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.screen = screen;
        this.backCamera = backCamera;
        this.frontCamera = frontCamera;
        this.img = img;
        this.desc = desc;
        this.type = type;
    };
};

let getProduct = () => {
    let getId = document.getElementById("getId").innerHTML;
    let getName = document.getElementById("getName").value;
    let getType = document.getElementById("getType").value;
    let getPrice = document.getElementById("getPrice").value;
    let getImg = document.getElementById("getImg").value;
    let getScreen = document.getElementById("getScreen").value;
    let getfrontCam = document.getElementById("getfrontCam").value;
    let getbackCam = document.getElementById("getbackCam").value;
    let getDesc = document.getElementById("getDesc").value;
    let Productsnewadd = new newProducts(getId, getName, getPrice, getScreen, getbackCam, getfrontCam, getImg, getDesc, getType);
    if (getId == "") {
        getId = Products.length;
        axios({
            url: `${Base_URL}Products`,
            method: "POST",
            data: Productsnewadd,
        })
            .then(function (response) {
                getData();
                document.getElementById("resetForm").click();
                document.getElementById("closeModal").click();
                alert("Đã thêm thành công");
            })
            .catch(function (error) {
                console.log(error);
            });
    } else {
        axios({
            url: `${Base_URL}Products/${getId}`,
            method: "PUT",
            data: Productsnewadd,
        })
            .then(function (response) {
                getData();
                document.getElementById("resetForm").click();
                document.getElementById("closeModal").click();
                alert("Đã cập nhật thành công");
            })
            .catch(function (error) {
                console.log(error);
            });
    };
}





let Delete = (id) => {
    axios({
        url: `${Base_URL}Products/${id}`,
        method: "DELETE",
    })
        .then(function (response) {
            getData();
            alert("Đã xoá thành công");
        })
        .catch(function (error) {
            console.log(error);
        });
}