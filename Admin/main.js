// link api
const Base_URL = "https://637af59f10a6f23f7f9bf2cc.mockapi.io/";

// object product
// class Products {
//     constructor(id, name,price,screen,backCamera, frontCamera,img,desc,type){
//         this.id = id;
//         this.name = name;
//         this.price = price;
//         this.screen = screen;
//         this.backCamera = backCamera;
//         this.frontCamera = frontCamera;
//         this.img = img;
//         this.desc = desc;
//         this.type = type;
//     };
// };


let Products = [];

// function get data
function getData() {

    axios({
        url: `${Base_URL}Products`,
        method: "GET",
    })
        .then((res) => {
            Products = res.data;
        })
        .catch(function (err) {
            console.log("err: ", err);
        });
}

getData();
console.log(Products)

// hiện sản phẩm trên màn hình, và giỏ hàng
const renderProducts = () => {
    contentHtml1 = ``;

    for (let data of Products) {
        // var producta = new Products(data[i].id, data[i].name,data[i].price,data[i].screen,data[i].backCamera, data[i].frontCamera,data[i].img,data[i].desc,data[i].type);
        // console.log(producta)
        contentHtml1 += `
        <div id="Content_${data[i].id}" class="card_items col-3 text-center p-3">
            <div class="card_img" >
                <img src=${data[i].img}>
            </div>
            
            <div class="card_title mb-1">
                <h3 >Loại: <span id="TypeProduct${data[i].id}">${data[i].type}</span></h3>
                <h3>Tên: ${data[i].name}</h3>
                <h3>Giá: ${data[i].price} $</h3>
                <p>Màn hình: ${data[i].screen}</p>
                <p>Camera sau: ${data[i].backCamera}</p>
                <p>Camera trước: ${data[i].frontCamera}</p>
                <p>Miêu tả: ${data[i].desc}</p>

                <button onclick="console.log(${data[i].id})" type="button" class="btn btn-primary btn-sm my-1" data-toggle="modal" data-target="#exampleModal">Cập nhật</button>

                <button onclick="delete(${data[i].id})" type="button" class="btn btn-primary btn-sm">Xoá</button>
            </div>
        </div>
        `
    }

    document.getElementById("showProducts").innerHTML = contentHtml1;
};

renderProducts();


// function filter 
let filter = () => {
    let Valuefilter = document.getElementById("Valuefilter").value;
    let TypeProduct = (document.querySelectorAll(".card_title"));

    for (let i = 0; i < TypeProduct.length; i++) {
        document.getElementById(`Content_${i}`).style.display = "";
        if (Valuefilter == 2) {
            if (document.getElementById(`TypeProduct${i}`).innerHTML == "Samsung") {
                document.getElementById(`Content_${i}`).style.display = "none";
            }
        }
        if (Valuefilter == 3) {
            if (document.getElementById(`TypeProduct${i}`).innerHTML == "Iphone") {
                document.getElementById(`Content_${i}`).style.display = "none";
            }
        }
    }
}

// function update thông tin của sản phẩm
let update = (item) => {
    return item
}

// function show thông tin cần sản phẩm cần update
// let showinforProducts = () => {
//     console.log(item)
// }